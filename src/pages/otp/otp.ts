import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, App } from 'ionic-angular';
//import { RegisterPage } from '../register/register';
import { TabsPage } from '../tabs/tabs';
import { LoginPage } from '../login/login';

import { ChamaProvider } from '../../providers/chama/chama';

import { SpinnerDialog } from '@ionic-native/spinner-dialog';

/**
 * Generated class for the OtpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-otp',
  templateUrl: 'otp.html',
})
export class OtpPage {

  loading: any;
  otpData = { otpCode:'' };
  data: any;

  constructor(private spinnerDialog: SpinnerDialog,public navCtrl: NavController, public navParams: NavParams,
  public alertCtrl: AlertController,public Chamaservice: ChamaProvider, private app: App) {
    if(!localStorage.getItem("DigitalTrustuserstatus")) { } else {
    if(localStorage.getItem("DigitalTrustuserstatus") == "loggedin") {
      this.navCtrl.setRoot(TabsPage);
    }
    else if(!localStorage.getItem("DigitalTrustcustomerphone")) {

      }
      else {
        // console.log("login 2222222")
        // if(!localStorage.getItem("DigitalTrustusersetpin")) {
        //   this.navCtrl.setRoot(SetpinPage);
        // }
        // else {
        //   this.navCtrl.setRoot(TabsPage);
        // }
      }
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OtpPage');
  }

  ActionLogin() {

    //var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  if(navigator.onLine === false) {
    console.log("not online")
    this.presentAlert('Internet Connection',"Please put data connection on and try again");
  }
  else if (this.otpData.otpCode === "") {
    this.presentAlert('',"All fields must be filled");
      }
  //     else if(!re.test(this.otpData.phone)) {
  //   // Invalid phone
  //   this.presentAlert('',"Invalid phone address");
  // }
      else {

this.spinnerDialog.show();//this.spinnerDialog.hide();
  this.Chamaservice.Login(this.otpData).then((result) => {
    this.spinnerDialog.hide();
    this.data = result;
    localStorage.setItem('token', this.data.access_token);
    console.log("brian-----"+JSON.stringify(this.data));
    let status_code = this.data.status_code;
    let message = this.data.message;

    if(status_code == "200") {

      let customername = this.data.first_name+" "+this.data.last_name;
      let customerfirstname = this.data.first_name;
      let customerlastname = this.data.last_name;
      let countyname = this.data.countyname;
      let town = this.data.town;
      let picture = this.data.picture;
      let phone = this.data.phonenumber;

      localStorage.setItem("DigitalTrustuserstatus","loggedin")
      localStorage.setItem("DigitalTrustcustomerphone",phone)
      localStorage.setItem("DigitalTrustcustomercountyname",countyname)
      localStorage.setItem("DigitalTrustcustomertown",town)
      localStorage.setItem("DigitalTrustcustomername",customername)
      localStorage.setItem("DigitalTrustcustomerfirstname",customerfirstname)
      localStorage.setItem("DigitalTrustcustomerlastname",customerlastname)
      localStorage.setItem("DigitalTrustprofilepicture",picture)

      this.app.getRootNav().setRoot(TabsPage);
      //this.navCtrl.setRoot(TabsPage);

    }
    else {
      this.presentAlert("SMS Code",message);
    }

  }, (err) => {
    this.spinnerDialog.hide();
    this.presentAlert("Account","Sorry, we are experiencing a technical challenge. Try again later.");
  });

  }
  }



  presentAlert(alerttitle,alertcontent) {
  const alert = this.alertCtrl.create({
   title: alerttitle,
   subTitle: alertcontent,
   buttons: ['Ok']
  });
  alert.present();
  }


 //  openRegister() {
 //   this.navCtrl.push(RegisterPage);
 // }

 openLogin() {
  this.app.getRootNav().setRoot(LoginPage);
}

}
