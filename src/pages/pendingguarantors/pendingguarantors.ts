import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController, LoadingController,ToastController } from 'ionic-angular';
import { ChamaProvider } from '../../providers/chama/chama';
/**
 * Generated class for the GuarantorsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pendingguarantors',
  templateUrl: 'pendingguarantors.html',
})
export class PendingguarantorsPage {

  list:Array<any>;

  loading:any;
  data: any;
  errorMessage: string;
  page = 1;
  perPage = 0;
  totalData = 0;
  totalPage = 0;
  nodataavailable:boolean = false;//

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController,
    private loadingCtrl:LoadingController,public toastCtrl:ToastController,
  public Chamaservice: ChamaProvider) {
    this.loadpendingrequests();
  }

  loadpendingrequests() {

    if(navigator.onLine !== true) {
      console.log("not online")
      this.presentAlertNetwork('Internet Connection',"Please put data connection on and try again");
    }
    else {
      let loading = this.loadingCtrl.create({ duration: 5000, content: 'Loading...' });
  loading.present();
  this.Chamaservice.loadpendingrequests()
     .subscribe(
       res => {
         loading.dismissAll();
         this.data = res;
         console.log("pendingrequests --"+JSON.stringify(this.data));
         this.list = this.data.result;
         let obj = this.data.result;
         if(obj.length < 1) {
                  this.nodataavailable = true;
                }
       },
       error =>  this.errorMessage = <any>error);
}
  }

  // doInfinite2(infiniteScroll) {
  // this.page2 = this.page2+1;
  // setTimeout(() => {
  // this.gtservice.jobApplications(this.page2)
  //    .subscribe(
  //      res => {
  //        this.data = res;
  //        this.perPage2 = this.data.per_page;
  //        this.totalData2 = this.data.total;
  //        this.totalPage2 = this.data.total_pages;
  //        for(let i=0; i<this.data.data.length; i++) {
  //          this.list2.push(this.data.data[i]);
  //        }
  //      },
  //      error =>  this.errorMessage2 = <any>error);
  //
  // console.log('Async operation has ended');
  // infiniteScroll.complete();
  // }, 1000);
  // }

presentAlertNetwork(alerttitle,alertcontent) {
const alert = this.alertCtrl.create({
title: alerttitle,
subTitle: alertcontent,
buttons: ['Ok']
});
alert.present();
//alert.onDidDismiss(() => {  setTimeout(() => this.navCtrl.setRoot(TabsPage), 500); });
}

TakeActionAccept(id,status) {
  let alert = this.alertCtrl.create({
    title: 'Confirm',
    message: 'Do you want to accept this request?',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Yes',
        handler: () => {
          console.log('Yes clicked');
          this.ApplyThis(id,status);
        }
      }
    ]
  });
  alert.present();
}


TakeActionDecline(id,status) {
  let alert = this.alertCtrl.create({
    title: 'Confirm',
    message: 'Do you want to decline this request?',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Yes',
        handler: () => {
          console.log('Yes clicked');
          this.ApplyThis(id,status);
        }
      }
    ]
  });
  alert.present();
}


ApplyThis(id,status) {
  let loading = this.loadingCtrl.create({ content: 'Please wait...' });
  loading.present();

this.Chamaservice.actOnGuarantorRequest(id,status).then((result) => {

loading.dismissAll();

this.data = result;
console.log("actOnGuarantorRequest-----"+JSON.stringify(this.data));
let status_code = this.data.code;
let message = this.data.message;
if(status_code == 1) {

  this.loadpendingrequests();
  this.presentToast(message);

}
else {
  this.presentToast(message);
}

}, (err) => {
loading.dismissAll();
this.presentToast('Failed. Please try again later.');
});
}

presentToast(toastmessage) {
  let toast = this.toastCtrl.create({
    message: toastmessage,
    duration: 3000,
    position: 'bottom',
    cssClass: "toast-success"
  });

  toast.onDidDismiss(() => {
    console.log('Dismissed toast');
  });

  toast.present();
}


  ionViewDidLoad() {
    console.log('ionViewDidLoad GuarantorsPage');
  }

}
