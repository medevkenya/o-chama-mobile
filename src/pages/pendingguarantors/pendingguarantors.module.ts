import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PendingguarantorsPage } from './pendingguarantors';

@NgModule({
  declarations: [
    PendingguarantorsPage,
  ],
  imports: [
    IonicPageModule.forChild(PendingguarantorsPage),
  ],
})
export class PendingguarantorsPageModule {}
