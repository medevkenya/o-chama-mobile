import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import { ChamaProvider } from '../../providers/chama/chama';

import { RegisterPage } from '../register/register';
import { SpinnerDialog } from '@ionic-native/spinner-dialog';
import { HomePage } from '../home/home';
/**
 * Generated class for the availablegroupsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-availablegroups',
  templateUrl: 'availablegroups.html',
})
export class AvailablegroupsPage {

  loading:any;

  data: any;
  availablegroups: string[];
  errorMessage: string;
  page = 1;
  perPage = 0;
  totalData = 0;
  totalPage = 0;

  constructor(private spinnerDialog: SpinnerDialog,public alertCtrl: AlertController,public navCtrl: NavController,
    public navParams: NavParams,public Chamaservice: ChamaProvider) {
  //  this.getavailablegroups();
      }

      getavailablegroups() {
        if(navigator.onLine === false) {
          console.log("not online")
          this.presentAlertNetwork('Internet Connection',"Please put data connection on and try again");
        }
        else {
        this.spinnerDialog.show();//this.spinnerDialog.hide();
      this.Chamaservice.loadavailablegroups()
         .subscribe(
           res => {
             this.spinnerDialog.hide();
             this.data = res;
             console.log("loadavailablegroups --"+JSON.stringify(this.data));
             this.availablegroups = this.data.result;
             // this.perPage = this.data.per_page;
             // this.totalData = this.data.total;
             // this.totalPage = this.data.total_pages;
           },
           error =>  this.errorMessage = <any>error);
    }
  }

  presentAlertNetwork(alerttitle,alertcontent) {
  const alert = this.alertCtrl.create({
   title: alerttitle,
   subTitle: alertcontent,
   buttons: ['Ok']
  });
  alert.present();
  alert.onDidDismiss(() => {  setTimeout(() => this.navCtrl.setRoot(HomePage), 500); });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad availablegroupsPage');
    this.getavailablegroups();
  }


  presentConfirm(event,listitem) {
    let alert = this.alertCtrl.create({
      title: 'Confirm',
      message: 'Do you want to join team '+listitem.name+' for Ksh. '+listitem.dailyAmount+' daily contribution?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            console.log('Yes clicked');
            this.openDetails(event,listitem);
          }
        }
      ]
    });
    alert.present();
  }

openDetails(event,listitem) {
 this.navCtrl.push(RegisterPage, {
 id: listitem.id,
 serialNumber: listitem.serialNumber,
 name: listitem.name,
 dailyAmount: listitem.dailyAmount
 });
 console.log("name---"+listitem.name);
 localStorage.setItem('DigitalTrustactivegroupid',listitem.id)
}

}
