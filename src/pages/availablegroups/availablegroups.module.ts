import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AvailablegroupsPage } from './availablegroups';

@NgModule({
  declarations: [
    AvailablegroupsPage,
  ],
  imports: [
    IonicPageModule.forChild(AvailablegroupsPage),
  ],
})
export class AvailablegroupsPageModule {}
