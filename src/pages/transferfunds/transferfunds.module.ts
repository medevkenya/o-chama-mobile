import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TransferfundsPage } from './transferfunds';

@NgModule({
  declarations: [
    TransferfundsPage,
  ],
  imports: [
    IonicPageModule.forChild(TransferfundsPage),
  ],
})
export class TransferfundsPageModule {}
