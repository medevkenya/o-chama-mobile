import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PreaddPage } from './preadd';

@NgModule({
  declarations: [
    PreaddPage,
  ],
  imports: [
    IonicPageModule.forChild(PreaddPage),
  ],
})
export class PreaddPageModule {}
