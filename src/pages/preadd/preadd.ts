import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import { ChamaProvider } from '../../providers/chama/chama';

import { AddPage } from '../add/add';
import { SpinnerDialog } from '@ionic-native/spinner-dialog';
import { HomePage } from '../home/home';
/**
 * Generated class for the preaddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-preadd',
  templateUrl: 'preadd.html',
})
export class PreaddPage {

  loading:any;

  data: any;
  preadd: string[];
   errorMessage: string;
  // page = 1;
  // perPage = 0;
  // totalData = 0;
  // totalPage = 0;

  constructor(private spinnerDialog: SpinnerDialog,public alertCtrl: AlertController,public navCtrl: NavController,
    public navParams: NavParams,public Chamaservice: ChamaProvider) {
  //  this.getpreadd();
      }

      getpreadd() {
        if(navigator.onLine === false) {
          console.log("not online")
          this.presentAlertNetwork('Internet Connection',"Please put data connection on and try again");
        }
        else {
        this.spinnerDialog.show();//this.spinnerDialog.hide();
      this.Chamaservice.loadpreadd()
         .subscribe(
           res => {
             this.spinnerDialog.hide();
             this.data = res;
             console.log("loadpreadd --"+JSON.stringify(this.data));
             this.preadd = this.data.result;
             // this.perPage = this.data.per_page;
             // this.totalData = this.data.total;
             // this.totalPage = this.data.total_pages;
           },
           error =>  this.errorMessage = <any>error);
    }
  }

  presentAlertNetwork(alerttitle,alertcontent) {
  const alert = this.alertCtrl.create({
   title: alerttitle,
   subTitle: alertcontent,
   buttons: ['Ok']
  });
  alert.present();
  alert.onDidDismiss(() => {  setTimeout(() => this.navCtrl.setRoot(HomePage), 500); });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad preaddPage');
    this.getpreadd();
  }

  openDetails(event,listitem) {
   this.navCtrl.push(AddPage, {
   id: listitem.id,
   serialNumber: listitem.serialNumber,
   name: listitem.name,
   dailyAmount: listitem.dailyAmount
   });
   console.log("name---"+listitem.name);
   localStorage.setItem('DigitalTrustactivegroupid',listitem.id)
  }

// doInfinite(infiniteScroll) {
//   this.page = this.page+1;
//   setTimeout(() => {
//     this.Chamaservice.loadpreadd(this.page)
//        .subscribe(
//          res => {
//            this.data = res;
//            this.perPage = this.data.per_page;
//            this.totalData = this.data.total;
//            this.totalPage = this.data.total_pages;
//            for(let i=0; i<this.data.data.length; i++) {
//              this.preadd.push(this.data.data[i]);
//            }
//          },
//          error =>  this.errorMessage = <any>error);
//
//     console.log('Async operation has ended');
//     infiniteScroll.complete();
//   }, 1000);
// }

}
