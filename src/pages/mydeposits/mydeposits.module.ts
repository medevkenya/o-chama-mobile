import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MydepositsPage } from './mydeposits';

@NgModule({
  declarations: [
    MydepositsPage,
  ],
  imports: [
    IonicPageModule.forChild(MydepositsPage),
  ],
})
export class MydepositsPageModule {}
