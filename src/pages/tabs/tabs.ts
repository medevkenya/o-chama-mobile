import { Component } from '@angular/core';

import { NavParams } from 'ionic-angular';

import { AboutPage } from '../about/about';
import { PrivacyPage } from '../privacy/privacy';
//import { HomePage } from '../home/home';
import { DashboardPage } from '../dashboard/dashboard';
import { FaqsPage } from '../faqs/faqs';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  // set the root pages for each tab
  tab1Root: any = DashboardPage;
  tab2Root: any = AboutPage;
  tab3Root: any = FaqsPage;
  tab4Root: any = PrivacyPage;
  mySelectedIndex: number;

  //gtsprofilepicture:any;

  constructor(navParams: NavParams) {
    this.mySelectedIndex = navParams.data.tabIndex || 0;
  }

  // Logout() {
  //   var navOptions = {animation: 'ios-transition'};
  //   this.navCtrl.push(HomePage, null, navOptions);
  // }

  ionViewWillLeave() {
    console.log("Looks like I'm about to leave :(");
  }

  ionViewDidEnter() {
    console.log("Looks like I have entered this page TabsPage :(");
    //this.gtsprofilepicture = localStorage.getItem('gtsprofilepicture');

  }

}
