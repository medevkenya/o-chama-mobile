import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController,LoadingController } from 'ionic-angular';
import { ChamaProvider } from '../../providers/chama/chama';

import { TransferfundsPage } from '../transferfunds/transferfunds';
import { WithdrawcashPage } from '../withdrawcash/withdrawcash';

/**
 * Generated class for the WalletPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-wallet',
  templateUrl: 'wallet.html',
})
export class WalletPage {

  availableBalance:any;

  data:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public alertCtrl: AlertController,public Chamaservice: ChamaProvider, private loadingCtrl:LoadingController) {
      this.CheckWalletBalance();
  }

  CheckWalletBalance(){
    if(navigator.onLine === false) {
      console.log("not online")
      this.presentAlertNetwork('Internet Connection',"Please put data connection on and try again");
    }
    else {
      let loading = this.loadingCtrl.create({ content: 'Please Wait...' });
          loading.present();
  this.Chamaservice.CheckWalletBalance()
  .then(result => {
    this.data = result;
      loading.dismissAll();
    this.availableBalance = this.data.result.amount;//mm
  });
}
  }

presentAlertNetwork(alerttitle,alertcontent) {
const alert = this.alertCtrl.create({
title: alerttitle,
subTitle: alertcontent,
buttons: ['Ok']
});
alert.present();
//alert.onDidDismiss(() => {  setTimeout(() => this.navCtrl.setRoot(TabsPage), 500); });
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad WalletPage');
  }

  openWithdrawCash() {
    var navOptions = {animation: 'ios-transition'};
    this.navCtrl.push(WithdrawcashPage, null, navOptions);
  }

  openTransfer() {
    var navOptions = {animation: 'ios-transition'};
    this.navCtrl.push(TransferfundsPage, null, navOptions);
  }

}
