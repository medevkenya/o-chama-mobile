import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GuarantorsPage } from './guarantors';

@NgModule({
  declarations: [
    GuarantorsPage,
  ],
  imports: [
    IonicPageModule.forChild(GuarantorsPage),
  ],
})
export class GuarantorsPageModule {}
