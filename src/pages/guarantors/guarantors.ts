import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController, LoadingController,ToastController } from 'ionic-angular';
import { ChamaProvider } from '../../providers/chama/chama';
/**
 * Generated class for the GuarantorsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-guarantors',
  templateUrl: 'guarantors.html',
})
export class GuarantorsPage {

  list:Array<any>;

  loading:any;
  data: any;
  errorMessage: string;
  page = 1;
  perPage = 0;
  totalData = 0;
  totalPage = 0;
  nodataavailable:boolean = false;//

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController,
    private loadingCtrl:LoadingController,public toastCtrl:ToastController,
  public Chamaservice: ChamaProvider) {
    this.loadmyguarantors();
  }

  loadmyguarantors() {

    if(navigator.onLine !== true) {
      console.log("not online")
      this.presentAlertNetwork('Internet Connection',"Please put data connection on and try again");
    }
    else {
      let loading = this.loadingCtrl.create({ duration: 5000, content: 'Loading...' });
  loading.present();
  this.Chamaservice.loadmyguarantors()
  .then(result => {
    this.data = result;
    loading.dismissAll();
    this.list = this.data.result;
    let obj = this.data.result;
    if(obj.length < 1) {
      this.nodataavailable = true;
    }
  });
  // this.Chamaservice.loadmyguarantors()
  //    .subscribe(
  //      res => {
  //        loading.dismissAll();
  //        this.data = res;
  //        console.log("loadmyguarantors --"+JSON.stringify(this.data));
  //        this.list = this.data.result;
  //        let obj = this.data.result;
  //        if(obj.length < 1) {
  //                 this.nodataavailable = true;
  //               }
  //      },
  //      error =>  this.errorMessage = <any>error);
}
  }



presentAlertNetwork(alerttitle,alertcontent) {
const alert = this.alertCtrl.create({
title: alerttitle,
subTitle: alertcontent,
buttons: ['Ok']
});
alert.present();
//alert.onDidDismiss(() => {  setTimeout(() => this.navCtrl.setRoot(TabsPage), 500); });
}


  ionViewDidLoad() {
    console.log('ionViewDidLoad GuarantorsPage');
  }

}
