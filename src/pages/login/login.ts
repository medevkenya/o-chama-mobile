import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, App } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { DashboardPage } from '../dashboard/dashboard';
import { ForgotpinPage } from '../forgotpin/forgotpin';
import { SetpinPage } from '../setpin/setpin';
import { ChamaProvider } from '../../providers/chama/chama';

import { SpinnerDialog } from '@ionic-native/spinner-dialog';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  loading: any;
  LoginData = { phone:'', pin:'' };
  data: any;

  constructor(private spinnerDialog: SpinnerDialog,public navCtrl: NavController, public navParams: NavParams,
  public alertCtrl: AlertController,public Chamaservice: ChamaProvider, private app: App) {

  if(!localStorage.getItem("DigitalTrustuserstatus")) { } else {
  if(localStorage.getItem("DigitalTrustuserstatus") == "loggedin") {
    //console.log("login 6666666666")
    this.navCtrl.setRoot(DashboardPage);
  }
  else if(!localStorage.getItem("DigitalTrustcustomerphone")) {

    }
    else {

    }
  }


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    //this.LoginData.phone = localStorage.getItem('DigitalTrustcustomerphone')
  }

  ActionLogin() {

    //var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  if(navigator.onLine === false) {
    console.log("not online")
    this.presentAlert('Internet Connection',"Please put data connection on and try again");
  }
  else if (this.LoginData.phone === "") {
    this.presentAlert('',"All fields must be filled");
      }
  //     else if(!re.test(this.LoginData.phone)) {
  //   // Invalid phone
  //   this.presentAlert('',"Invalid phone address");
  // }
      else {

this.spinnerDialog.show();
  this.Chamaservice.Login(this.LoginData).then((result) => {
    this.spinnerDialog.hide();
    this.data = result;
    localStorage.setItem('token', this.data.access_token);
    console.log("brian login-----"+JSON.stringify(this.data));
    let status_code = this.data.code;
    let message = this.data.message;

    if(status_code == 1) {

       let customerfirstname = this.data.result.member.firstName;
       let customerlastname = this.data.result.member.lastName;
       //let county = this.data.result.member.county;
       let pinSet = this.data.result.member.pinSet;
      // let picture = this.data.picture;
      let phone = this.data.result.member.phone;

      localStorage.setItem("DigitalTrustuserstatus","loggedin")
      localStorage.setItem("DigitalTrustcustomerphone",phone)
      //localStorage.setItem("DigitalTrustcustomercounty",county)
       localStorage.setItem("DigitalTrustcustomerfirstname",customerfirstname)
       localStorage.setItem("DigitalTrustcustomerlastname",customerlastname)

      if(pinSet == 0) {
        this.app.getRootNav().setRoot(SetpinPage);
      }
      else {
        this.app.getRootNav().setRoot(DashboardPage);
      }
      //this.presentAlert("DigitalTrust",message);

    }
    else {
      this.presentAlert("DigitalTrust",message);
    }

  }, (err) => {
    this.spinnerDialog.hide();
    this.presentAlert("Account","Failed to login. Try again later.");
  });

  }
  }


  presentAlert(alerttitle,alertcontent) {
  const alert = this.alertCtrl.create({
   title: alerttitle,
   subTitle: alertcontent,
   buttons: ['Ok']
  });
  alert.present();
  }

  openRegister() {
   this.navCtrl.push(RegisterPage);
 }

 openForgotpin() {
  this.navCtrl.push(ForgotpinPage);
}

// forgotPass() {
//   let forgot = this.alertCtrl.create({
//     title: 'Forgot Password?',
//     message: "Enter your phone address to send a reset link.",
//     inputs: [
//       {
//         name: 'phone',
//         placeholder: 'phone',
//         type: 'phone'
//       },
//     ],
//     buttons: [
//       {
//         text: 'Cancel',
//         handler: data => {
//         console.log('Cancel clicked--'+data);
//         }
//       },
//       {
//         text: 'Send',
//         handler: data => {
//           console.log('Send clicked---'+data.phone);
//           console.log("phone-----"+JSON.stringify(data));
//           if(data.phone =="" || data.phone==null) {} else {
//           this.ForgotPassword(data.phone);
//         }
//           // let toast = this.toastCtrl.create({
//           //   message: 'phone was sent successfully',
//           //   duration: 5000,
//           //   position: 'top',
//           //   cssClass: 'dark-trans',
//           //   closeButtonText: 'OK',
//           //   showCloseButton: true
//           // });
//           // toast.present();
//         }
//       }
//     ]
//   });
//   forgot.present();
// }
//
// ForgotPassword(phone) {
//   var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
//
//   if(navigator.onLine === false) {
//     console.log("not online")
//     this.presentAlert('',"Please put data connection on and try again");
//   }
//       else if(!re.test(phone)) {
//     // Invalid phone
//     this.presentAlert('',"Invalid phone address");
//   }
//       else {
//
// this.spinnerDialog.show();//this.spinnerDialog.hide();
//   this.Chamaservice.Forgotpin(phone).then((result) => {
//     this.spinnerDialog.hide();
//     this.data = result;
//     localStorage.setItem('token', this.data.access_token);
//     console.log("brian forgotpin-----"+JSON.stringify(this.data));
//     let status_code = this.data.status_code;
//     let message = this.data.message;
//     if(status_code == "200") {
//       this.presentAlert('',message);
//     }
//     else {
//       this.presentAlert("",message);
//     }
//
//   }, (err) => {
//     this.spinnerDialog.hide();
//     this.presentAlert("Account","Sorry, we are experiencing a technical challenge at the moment");
//   });
//
//   }
// }

}
