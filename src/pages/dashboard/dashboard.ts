import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, App,MenuController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';

import { MyaccountsPage } from '../myaccounts/myaccounts';

import { AvailablegroupsPage } from '../availablegroups/availablegroups';
import { HelpPage } from '../help/help';
import { MymessagesPage } from '../mymessages/mymessages';
import { HomePage } from '../home/home';

import { AboutPage } from '../about/about';
import { PrivacyPage } from '../privacy/privacy';
import { FaqsPage } from '../faqs/faqs';

import { TermsPage } from '../terms/terms';
import { ProfilePage } from '../profile/profile';

import { ChangelanguagePage } from '../changelanguage/changelanguage';
import { JoinnewgroupPage } from '../joinnewgroup/joinnewgroup';
import { PreaddPage } from '../preadd/preadd';


import { ChangepinPage } from '../changepin/changepin';
import { WalletPage } from '../wallet/wallet';
import { MinistatementPage } from '../ministatement/ministatement';

/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {

  constructor(public alertCtrl:AlertController,private socialSharing: SocialSharing,public navCtrl: NavController,
    public navParams: NavParams,public modalCtrl: ModalController, private app: App,public menuCtrl: MenuController) {
  }

  openMenu() {
   this.menuCtrl.open();
 }

  Logout() {
    const confirm = this.alertCtrl.create({
          title: 'Logout?',
          message: 'Do you want to exit this app?',
          buttons: [
            {
              text: 'Cancel',
              handler: () => {
                console.log('Cancel clicked');
              }
            },
            {
              text: 'Accept',
              handler: () => {
                console.log('Accept clicked');
                localStorage.removeItem("DigitalTrustuserstatus");
                 //var navOptions = {animation: 'ios-transition'};
                 this.app.getRootNav().setRoot(HomePage);
                 //this.navCtrl.setRoot(HomePage, null, navOptions);
              }
            }
          ]
        });
        confirm.present();
      }

  openMyAccount() {
    var navOptions = {animation: 'ios-transition'};
    this.navCtrl.push(MyaccountsPage, null, navOptions);
  }

  openAddMember() {
    var navOptions = {animation: 'ios-transition'};
    this.navCtrl.push(PreaddPage, null, navOptions);
  }

  openMyMessages() {
    this.menuCtrl.close();
    var navOptions = {animation: 'ios-transition'};
    this.navCtrl.push(MymessagesPage, null, navOptions);
  }

  openChangeLanguage() {
    this.menuCtrl.close();
    var navOptions = {animation: 'ios-transition'};
    this.navCtrl.push(ChangelanguagePage, null, navOptions);
  }

  openCustomerCare() {
    var navOptions = {animation: 'ios-transition'};
    this.navCtrl.push(HelpPage, null, navOptions);
  }

  openJoinNewGroup() {
    var navOptions = {animation: 'ios-transition'};
    this.navCtrl.push(JoinnewgroupPage, null, navOptions);
  }

  openAvailableGroups() {
    var navOptions = {animation: 'ios-transition'};
    this.navCtrl.push(AvailablegroupsPage, null, navOptions);
  }

  openChangePIN() {
    this.menuCtrl.close();
    var navOptions = {animation: 'ios-transition'};
    this.navCtrl.push(ChangepinPage, null, navOptions);
  }

  openWallet() {
    var navOptions = {animation: 'ios-transition'};
    this.navCtrl.push(WalletPage, null, navOptions);
  }

  openMinistatement() {
    var navOptions = {animation: 'ios-transition'};
    this.navCtrl.push(MinistatementPage, null, navOptions);
  }

  openTerms() {
    this.menuCtrl.close();
    var navOptions = {animation: 'ios-transition'};
    this.navCtrl.push(TermsPage, null, navOptions);
  }

  openPolicy() {
    this.menuCtrl.close();
    var navOptions = {animation: 'ios-transition'};
    this.navCtrl.push(PrivacyPage, null, navOptions);
  }

  openAbout() {
    this.menuCtrl.close();
    var navOptions = {animation: 'ios-transition'};
    this.navCtrl.push(AboutPage, null, navOptions);
  }

  openFaqs() {
    this.menuCtrl.close();
    var navOptions = {animation: 'ios-transition'};
    this.navCtrl.push(FaqsPage, null, navOptions);
  }

  openProfile() {
    var navOptions = {animation: 'ios-transition'};
    this.navCtrl.push(ProfilePage, null, navOptions);
  }


ShareByEmail() {
    // Check if sharing via email is supported
this.socialSharing.canShareViaEmail().then(() => {
  // Sharing via email is possible
  // Share via email
  let emailbody = 'DigitalTrust is digital mobile based platform where every registered member has been provided with an avenue to grow economically through a community based approach. All members are guaranteed 100% to get the contribution of the group multiple times. We endorse a fair and transparent avenue for gaining and improving the contributions step by step. Register by sending SMS with the word CHAMA to 20879. Pay through MPESA pay bill number 965671. Thank you.';

  this.socialSharing.shareViaEmail(''+emailbody+'', 'DigitalTrust', ['info@DigitalTrust.com']).then(() => {
    // Success!
  }).catch(() => {
    // Error!
  });
}).catch(() => {
  // Sharing via email is not possible
});


}


whatsappShare(index){
  var msg  = 'DigitalTrust is digital mobile based platform where every registered member has been provided with an avenue to grow economically through a community based approach. All members are guaranteed 100% to get the contribution of the group multiple times. We endorse a fair and transparent avenue for gaining and improving the contributions step by step. Register by sending SMS with the word CHAMA to 20879. Pay through MPESA pay bill number 965671. Thank you.';
//this.compilemsg(index);
   this.socialSharing.shareViaWhatsApp(msg, null, null);
}

twitterShare(index){
  var msg  = 'DigitalTrust is digital mobile based platform where every registered member has been provided with an avenue to grow economically through a community based approach. All members are guaranteed 100% to get the contribution of the group multiple times. We endorse a fair and transparent avenue for gaining and improving the contributions step by step. Register by sending SMS with the word CHAMA to 20879. Pay through MPESA pay bill number 965671. Thank you.';
//this.compilemsg(index);
  this.socialSharing.shareViaTwitter(msg, null, null);
}

facebookShare(index){
   var msg  = 'DigitalTrust is digital mobile based platform where every registered member has been provided with an avenue to grow economically through a community based approach. All members are guaranteed 100% to get the contribution of the group multiple times. We endorse a fair and transparent avenue for gaining and improving the contributions step by step. Register by sending SMS with the word CHAMA to 20879. Pay through MPESA pay bill number 965671. Thank you.';
//this.compilemsg(index);
    this.socialSharing.shareViaFacebook(msg, null, null);
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad DashboardPage');
  }

}
