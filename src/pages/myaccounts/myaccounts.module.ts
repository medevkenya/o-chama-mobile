import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyaccountsPage } from './myaccounts';

@NgModule({
  declarations: [
    MyaccountsPage,
  ],
  imports: [
    IonicPageModule.forChild(MyaccountsPage),
  ],
})
export class MyaccountsPageModule {}
