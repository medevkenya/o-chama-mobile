import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController,LoadingController, App } from 'ionic-angular';
import { ChamaProvider } from '../../providers/chama/chama';
import { HomePage } from '../home/home';
/**
 * Generated class for the ChangepinPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-changepin',
  templateUrl: 'changepin.html',
})
export class ChangepinPage {

  loading: any;
  ChangePinData = { currentpin:'', pin:'', confirmpin:'' };
  data: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public alertCtrl: AlertController,public Chamaservice: ChamaProvider, private loadingCtrl:LoadingController, private app: App) {
  }

  ActionChangePin() {

    if(navigator.onLine === false) {
      console.log("not online")
      this.presentAlertNetwork('Internet Connection',"Please put data connection on and try again");
    }
  else if (this.ChangePinData.currentpin === "" || this.ChangePinData.confirmpin === "" || this.ChangePinData.pin === "") {
    this.presentAlert('',"All fields must be filled");
      }
      else if (isNaN(+this.ChangePinData.currentpin)) {
      this.presentAlert('',"Current PIN is invalid");
      }
      else if (this.ChangePinData.currentpin.length !=4) {
      this.presentAlert('',"Current PIN is invalid. Must be 4 digits");
      }
      else if (isNaN(+this.ChangePinData.pin)) {
      this.presentAlert('',"PIN is invalid");
      }
      else if (this.ChangePinData.pin.length !=4) {
      this.presentAlert('',"PIN is invalid. Must be 4 digits");
      }
      else if (isNaN(+this.ChangePinData.confirmpin)) {
      this.presentAlert('',"Confirm PIN is invalid");
      }
      else if (this.ChangePinData.pin.length !=4) {
      this.presentAlert('',"Confirm PIN is invalid. Must be 4 digits");
      }
      else if (this.ChangePinData.pin !== this.ChangePinData.confirmpin) {
      this.presentAlert('',"PIN fields do not match");
      }
      else {

        let loading = this.loadingCtrl.create({ content: 'Please Wait...' });
        loading.present();

  this.Chamaservice.ChangePin(this.ChangePinData).then((result) => {
    loading.dismissAll();
    this.data = result;
    localStorage.setItem('token', this.data.access_token);
    console.log("brian-----"+JSON.stringify(this.data));
    let status_code = this.data.code;
    let message = this.data.message;
    if(status_code == 1) {

      //clear fields
      this.ChangePinData.pin === "";
      this.ChangePinData.confirmpin === "";
      this.ChangePinData.currentpin === "";

        this.presentAlert('',message);
        localStorage.removeItem("DigitalTrustuserstatus");
         this.app.getRootNav().setRoot(HomePage);

    }
    else {
      this.presentAlert('',message);
    }

  }, (err) => {
    loading.dismissAll();
    this.presentAlert('',"Failed to change PIN. Please try again later.");
  });

}
}


presentAlert(alerttitle,alertcontent) {
 const alert = this.alertCtrl.create({
   title: alerttitle,
   subTitle: alertcontent,
   buttons: ['Ok']
 });
 alert.present();
}

presentAlertNetwork(alerttitle,alertcontent) {
const alert = this.alertCtrl.create({
title: alerttitle,
subTitle: alertcontent,
buttons: ['Ok']
});
alert.present();
//alert.onDidDismiss(() => {  setTimeout(() => this.navCtrl.setRoot(TabsPage), 500); });
}


  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangepinPage');
  }

}
