import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import { ChamaProvider } from '../../providers/chama/chama';

//import { MypaymentsPage } from '../mypayments/mypayments';
import { SpinnerDialog } from '@ionic-native/spinner-dialog';
import { HomePage } from '../home/home';
/**
 * Generated class for the mymessagesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mymessages',
  templateUrl: 'mymessages.html',
})
export class MymessagesPage {

  loading:any;

  data: any;
  mymessages: string[];
  errorMessage: string;
  page = 1;
  perPage = 0;
  totalData = 0;
  totalPage = 0;

  constructor(private spinnerDialog: SpinnerDialog,public alertCtrl: AlertController,public navCtrl: NavController,
    public navParams: NavParams,public Chamaservice: ChamaProvider) {
  //  this.getmymessages();
      }

      getmymessages() {
        if(navigator.onLine === false) {
          console.log("not online")
          this.presentAlertNetwork('Internet Connection',"Please put data connection on and try again");
        }
        else {
        this.spinnerDialog.show();//this.spinnerDialog.hide();
        this.Chamaservice.loadmymessages()
        .then(result => {
          this.data = result;
          this.spinnerDialog.hide();
          this.mymessages = this.data.result;//mm
        });
    }
  }



  presentAlertNetwork(alerttitle,alertcontent) {
  const alert = this.alertCtrl.create({
   title: alerttitle,
   subTitle: alertcontent,
   buttons: ['Ok']
  });
  alert.present();
  alert.onDidDismiss(() => {  setTimeout(() => this.navCtrl.setRoot(HomePage), 500); });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad mymessagesPage');
    this.getmymessages();
  }


// doInfinite(infiniteScroll) {
//   this.page = this.page+1;
//   setTimeout(() => {
//     this.Chamaservice.loadmymessages(this.page)
//        .subscribe(
//          res => {
//            this.data = res;
//            this.perPage = this.data.per_page;
//            this.totalData = this.data.total;
//            this.totalPage = this.data.total_pages;
//            for(let i=0; i<this.data.data.length; i++) {
//              this.mymessages.push(this.data.data[i]);
//            }
//          },
//          error =>  this.errorMessage = <any>error);
//
//     console.log('Async operation has ended');
//     infiniteScroll.complete();
//   }, 1000);
// }

}
