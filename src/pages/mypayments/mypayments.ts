import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import { ChamaProvider } from '../../providers/chama/chama';

//import { MypaymentsPage } from '../mypayments/mypayments';
import { SpinnerDialog } from '@ionic-native/spinner-dialog';
import { HomePage } from '../home/home';
/**
 * Generated class for the mypaymentsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mypayments',
  templateUrl: 'mypayments.html',
})
export class MypaymentsPage {

  loading:any;

  data: any;
  mypayments: string[];
  errorMessage: string;
  page = 1;
  perPage = 0;
  totalData = 0;
  totalPage = 0;

  nodataavailable:boolean = false;

  constructor(private spinnerDialog: SpinnerDialog,public alertCtrl: AlertController,public navCtrl: NavController,
    public navParams: NavParams,public Chamaservice: ChamaProvider) {
  //  this.getmypayments();
      }

  //     getmypayments() {
  //       if(navigator.onLine === false) {
  //         console.log("not online")
  //         this.presentAlertNetwork('Internet Connection',"Please put data connection on and try again");
  //       }
  //       else {
  //       this.spinnerDialog.show();//this.spinnerDialog.hide();
  //     this.Chamaservice.loadmypayments(this.page)
  //        .subscribe(
  //          res => {
  //            this.spinnerDialog.hide();
  //            this.data = res;
  //            console.log("loadmypayments --"+JSON.stringify(this.data));
  //            this.mypayments = this.data.data;
  //            this.perPage = this.data.per_page;
  //            this.totalData = this.data.total;
  //            this.totalPage = this.data.total_pages;
  //          },
  //          error =>  this.errorMessage = <any>error);
  //   }
  // }


  getmypayments() {
  this.spinnerDialog.show();
  this.Chamaservice.loadmypayments(this.page)
  .then(result => {
    console.log("loadmypayments data---"+JSON.stringify(result))
    this.spinnerDialog.hide();
    this.data = result;
    this.mypayments = this.data.data;
    this.perPage = this.data.per_page;
    this.totalData = this.data.total;
    this.totalPage = this.data.total_pages;
    if(this.data.data == null) {
             this.nodataavailable = true;
           }
  });
}

  presentAlertNetwork(alerttitle,alertcontent) {
  const alert = this.alertCtrl.create({
   title: alerttitle,
   subTitle: alertcontent,
   buttons: ['Ok']
  });
  alert.present();
  alert.onDidDismiss(() => {  setTimeout(() => this.navCtrl.setRoot(HomePage), 500); });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad mypaymentsPage');
    this.getmypayments();
  }


doInfinite(infiniteScroll) {
  this.page = this.page+1;
  setTimeout(() => {
    this.Chamaservice.loadmypayments(this.page)
    .then(result => {
           this.data = result;
           this.perPage = this.data.per_page;
           this.totalData = this.data.total;
           this.totalPage = this.data.total_pages;
           for(let i=0; i<this.data.data.length; i++) {
             this.mypayments.push(this.data.data[i]);
           }
         },
         error =>  this.errorMessage = <any>error);

    console.log('Async operation has ended');
    infiniteScroll.complete();
  }, 1000);
}

}
