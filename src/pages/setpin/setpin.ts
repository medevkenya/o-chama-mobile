import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { ChamaProvider } from '../../providers/chama/chama';
import { LoginPage } from '../login/login';
import { SpinnerDialog } from '@ionic-native/spinner-dialog';
/**
 * Generated class for the setpinPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 @IonicPage()
 @Component({
   selector: 'page-setpin',
   templateUrl: 'setpin.html',
 })
 export class SetpinPage {

   loading: any;
   setpinData = { pin:'', confirmPin:'', };
   data: any;

   towns:any;
   counties:any;

   constructor(private spinnerDialog: SpinnerDialog,public navCtrl: NavController, public navParams: NavParams,
   public alertCtrl: AlertController,public Chamaservice: ChamaProvider) {

   }

   ionViewDidLoad() {
     console.log('ionViewDidLoad setpinPage');
   }

    Actionsetpin() {

      //var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

      if(navigator.onLine === false) {
        console.log("not online");
        this.presentAlertNetwork('Internet Connection',"Please put data connection on and try again");
      }
      else if (this.setpinData.pin === "" || this.setpinData.confirmPin === "") {
      this.presentAlert('',"All fields must be filled!");
      }
      else if (this.setpinData.pin != this.setpinData.confirmPin) {
      this.presentAlert('',"pin fields do not match!");
      }
      else {

      this.spinnerDialog.show();
      this.Chamaservice.Setpin(this.setpinData).then((result) => {
      this.spinnerDialog.hide();
      this.data = result;
      localStorage.setItem('token', this.data.access_token);
      console.log("brian-----"+JSON.stringify(this.data));
      let status_code = this.data.status_code;
      let message = this.data.message;
      if(status_code == "200") {

        //clear fields
        this.setpinData.pin === "";
        this.setpinData.confirmPin === "";//

        localStorage.setItem("DigitalTrustusersetpin","setpin")

          this.presentAlert('',message);
          this.navCtrl.push(LoginPage);
          return;

      }
      else {
        this.presentAlert('',message);
      }

    }, (err) => {
      this.spinnerDialog.hide();
      this.presentAlert('',"Failed to set pin. Please try again later.");
    });

  }
  }


  presentAlert(alerttitle,alertcontent) {
   const alert = this.alertCtrl.create({
     title: alerttitle,
     subTitle: alertcontent,
     buttons: ['Ok']
   });
   alert.present();
 }

 presentAlertNetwork(alerttitle,alertcontent) {
 const alert = this.alertCtrl.create({
  title: alerttitle,
  subTitle: alertcontent,
  buttons: ['Ok']
 });
 alert.present();
 //alert.onDidDismiss(() => {  setTimeout(() => this.navCtrl.setRoot(TabsPage), 500); });
 }


 }
