import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { ChamaProvider } from '../../providers/chama/chama';
import { SpinnerDialog } from '@ionic-native/spinner-dialog';
/**
 * Generated class for the profilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation./
 */

 @IonicPage()
 @Component({
   selector: 'page-profile',
   templateUrl: 'profile.html',
 })
 export class ProfilePage {

   loading: any;
   ProfileData = { firstname:'', lastname:'', town:'' };
   data: any;

   towns:any;
   counties:any;
   //countyname:any;

   constructor(private spinnerDialog: SpinnerDialog,public navCtrl: NavController, public navParams: NavParams,
   public alertCtrl: AlertController,public Chamaservice: ChamaProvider) {

   }

   ionViewDidLoad() {
     console.log('ionViewDidLoad profilePage');
     this.ProfileData.town = localStorage.getItem("DigitalTrustcustomertown")
     this.ProfileData.firstname = localStorage.getItem("DigitalTrustcustomerfirstname")
     this.ProfileData.lastname = localStorage.getItem("DigitalTrustcustomerlastname")
     this.loadAllCounties();
   }

   loadAllCounties(){
     if(navigator.onLine === false) {
       console.log("not online")
       this.presentAlertNetwork('Internet Connection',"Please put data connection on and try again");
     }
     else {
     this.spinnerDialog.show();
   this.Chamaservice.loadcounties()
   .then(data => {
     this.spinnerDialog.hide();
     this.counties = data;//mm
     console.log("counties---"+this.counties)
   });
 }
   }


    ActionProfile() {

      //var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

      if(navigator.onLine === false) {
        console.log("not online")
        this.presentAlertNetwork('Internet Connection',"Please put data connection on and try again");
      }
    else if (this.ProfileData.town === "" || this.ProfileData.firstname === "" || this.ProfileData.lastname === "") {
      this.presentAlert('',"All fields must be filled");
        }
        else {

      this.spinnerDialog.show();//this.spinnerDialog.hide();
      this.Chamaservice.Profile(this.ProfileData).then((result) => {
      this.spinnerDialog.hide();
      this.data = result;
      localStorage.setItem('token', this.data.access_token);
      console.log("brian-----"+JSON.stringify(this.data));
      let status_code = this.data.status_code;
      let message = this.data.message;

      if(status_code == "200") {

        let customerfirstname = this.data.firstname;
        let customerlastname = this.data.lastname;
        //let countyname = this.data.countyname;
        let town = this.data.town;

        //localStorage.setItem("DigitalTrustcustomercountyname",countyname)
        localStorage.setItem("DigitalTrustcustomertown",town)
        localStorage.setItem("DigitalTrustcustomername",customerfirstname+" "+customerlastname)
        localStorage.setItem("DigitalTrustcustomerfirstname",customerfirstname)
        localStorage.setItem("DigitalTrustcustomerlastname",customerlastname)

          this.presentAlert('',message);//bb
          return;
      }
      else {
        this.presentAlert('',message);
      }

    }, (err) => {
      this.spinnerDialog.hide();
      this.presentAlert('',"Sorry, we are experiencing a technical challenge. Try again later.");
    });

  }
  }


  presentAlert(alerttitle,alertcontent) {
   const alert = this.alertCtrl.create({
     title: alerttitle,
     subTitle: alertcontent,
     buttons: ['Ok']
   });
   alert.present();
 }

 presentAlertNetwork(alerttitle,alertcontent) {
 const alert = this.alertCtrl.create({
  title: alerttitle,
  subTitle: alertcontent,
  buttons: ['Ok']
 });
 alert.present();
 //alert.onDidDismiss(() => {  setTimeout(() => this.navCtrl.setRoot(TabsPage), 500); });
 }


 }
