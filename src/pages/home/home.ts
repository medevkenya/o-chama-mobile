import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { LoginPage } from '../login/login';
import { DashboardPage } from '../dashboard/dashboard';
//import { OtpPage } from '../otp/otp';
import { AvailablegroupsPage } from '../availablegroups/availablegroups';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

    if(!localStorage.getItem("DigitalTrustuserstatus")) { } else {
    if(localStorage.getItem("DigitalTrustuserstatus") == "loggedin") {
      this.navCtrl.setRoot(DashboardPage);
    }
    // else if(localStorage.getItem("DigitalTrustuserstatus") == "otprequested") {
    //   this.navCtrl.setRoot(OtpPage);
    // }
    else if(!localStorage.getItem("DigitalTrustcustomerphone")) {
      this.navCtrl.push(LoginPage);
      }
    // else if(!localStorage.getItem("DigitalTrustcustomerphone")) {
    //
    //   }
      else {

      }
    }
  }

  ionViewWillEnter() {

  }

  openLogin() {
      this.navCtrl.push(LoginPage);
    }

  openRegister() {
    this.navCtrl.push(AvailablegroupsPage);
  }

}
