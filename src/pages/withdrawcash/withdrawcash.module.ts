import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WithdrawcashPage } from './withdrawcash';

@NgModule({
  declarations: [
    WithdrawcashPage,
  ],
  imports: [
    IonicPageModule.forChild(WithdrawcashPage),
  ],
})
export class WithdrawcashPageModule {}
