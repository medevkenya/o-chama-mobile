import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController,LoadingController } from 'ionic-angular';
import { ChamaProvider } from '../../providers/chama/chama';

/**
 * Generated class for the WithdrawcashPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-withdrawcash',
  templateUrl: 'withdrawcash.html',
})
export class WithdrawcashPage {

  loading: any;
  WithdrawData = { amount:'', pin:'' };
  data: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public alertCtrl: AlertController,public Chamaservice: ChamaProvider, private loadingCtrl:LoadingController) {

  }

  ActionWithdraw() {

    //var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if(navigator.onLine === false) {
      console.log("not online")
      this.presentAlertNetwork('Internet Connection',"Please put data connection on and try again");
    }
  else if (this.WithdrawData.amount === "" || this.WithdrawData.pin === "") {
    this.presentAlert('',"All fields must be filled");
      }
      else if (isNaN(+this.WithdrawData.amount)) {
      this.presentAlert('',"Please enter a valid amount");
      }
      else if (isNaN(+this.WithdrawData.pin)) {
      this.presentAlert('',"Please enter a valid PIN");
      }
      else if (this.WithdrawData.pin.length !=4) {
      this.presentAlert('',"PIN is invalid. Must be 4 digits");
      }
      else {

        let loading = this.loadingCtrl.create({ content: 'Please Wait...' });
            loading.present();

  this.Chamaservice.WithdrawCash(this.WithdrawData).then((result) => {
    loading.dismissAll();
    this.data = result;
    localStorage.setItem('token', this.data.access_token);
    console.log("brian-----"+JSON.stringify(this.data));
    let status_code = this.data.code;
    let message = this.data.message;
    if(status_code ==1) {

      //clear fields
      this.WithdrawData.amount === "";
      this.WithdrawData.pin === "";

      this.presentAlert('',message);

    }
    else {
      this.presentAlert('',message);
    }

  }, (err) => {
    loading.dismissAll();
    this.presentAlert('',"Sorry, we are experiencing a technical challenge. Try again later.");
  });

}
}


presentAlert(alerttitle,alertcontent) {
 const alert = this.alertCtrl.create({
   title: alerttitle,
   subTitle: alertcontent,
   buttons: ['Ok']
 });
 alert.present();
}

presentAlertNetwork(alerttitle,alertcontent) {
const alert = this.alertCtrl.create({
title: alerttitle,
subTitle: alertcontent,
buttons: ['Ok']
});
alert.present();
//alert.onDidDismiss(() => {  setTimeout(() => this.navCtrl.setRoot(TabsPage), 500); });
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad WithdrawcashPage');
  }

}
