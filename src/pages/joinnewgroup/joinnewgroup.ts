import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import { ChamaProvider } from '../../providers/chama/chama';

import { SpinnerDialog } from '@ionic-native/spinner-dialog';
import { HomePage } from '../home/home';
import { MyaccountsPage } from '../myaccounts/myaccounts';
/**
 * Generated class for the joinnewgroupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-joinnewgroup',
  templateUrl: 'joinnewgroup.html',
})
export class JoinnewgroupPage {

  loading:any;

  data: any;
  joinnewgroup: string[];
  errorMessage: string;
  page = 1;
  perPage = 0;
  totalData = 0;
  totalPage = 0;

  constructor(private spinnerDialog: SpinnerDialog,public alertCtrl: AlertController,public navCtrl: NavController,
    public navParams: NavParams,public Chamaservice: ChamaProvider) {
  //  this.getjoinnewgroup();
      }

      getjoinnewgroup() {
        if(navigator.onLine === false) {
          console.log("not online")
          this.presentAlertNetwork('Internet Connection',"Please put data connection on and try again");
        }
        else {
        this.spinnerDialog.show();//this.spinnerDialog.hide();
      this.Chamaservice.loadjoinnewgroup()
         .subscribe(
           res => {
             this.spinnerDialog.hide();
             this.data = res;
             console.log("loadjoinnewgroup --"+JSON.stringify(this.data));
             this.joinnewgroup = this.data.result;
             // this.perPage = this.data.per_page;
             // this.totalData = this.data.total;
             // this.totalPage = this.data.total_pages;
           },
           error =>  this.errorMessage = <any>error);
    }
  }

  presentAlertNetwork(alerttitle,alertcontent) {
  const alert = this.alertCtrl.create({
   title: alerttitle,
   subTitle: alertcontent,
   buttons: ['Ok']
  });
  alert.present();
  alert.onDidDismiss(() => {  setTimeout(() => this.navCtrl.setRoot(HomePage), 500); });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad joinnewgroupPage');
    this.getjoinnewgroup();
  }



  presentConfirm(event,listitem) {
    let alert = this.alertCtrl.create({
      title: 'Confirm',
      message: 'Do you want to join this group?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            console.log('Yes clicked');
            this.openDetails(event,listitem);
          }
        }
      ]
    });
    alert.present();
  }


openDetails(event,listitem) {
 // paramsubgroupid: listitem.subgroupid,
 // paramsubgrouplabel: listitem.subgrouplabel,
 // paramgroupname: listitem.groupname,
 // paramsubgroupdaily: listitem.subgroupdaily

 if(navigator.onLine === false) {
   console.log("not online");
   this.presentAlertNetwork('Internet Connection',"Please put data connection on and try again");
 }
 else {

 this.spinnerDialog.show();
 this.Chamaservice.Joinnewgroup(listitem.id).then((result) => {
 this.spinnerDialog.hide();
 this.data = result;//
 localStorage.setItem('token', this.data.access_token);
 console.log("brian-----"+JSON.stringify(this.data));
 let status_code = this.data.code;
 let message = this.data.message;
 if(status_code == 1) {
       this.presentAlert('',message);
     this.navCtrl.push(MyaccountsPage);
     return;

 }
 else {
   this.presentAlert('',message);
 }

 }, (err) => {
 this.spinnerDialog.hide();
 this.presentAlert('',"Sorry, we are experiencing a technical challenge. Try again later.");
 });

 }

}

presentAlert(alerttitle,alertcontent) {
 const alert = this.alertCtrl.create({
   title: alerttitle,
   subTitle: alertcontent,
   buttons: ['Ok']
 });
 alert.present();
}

}
