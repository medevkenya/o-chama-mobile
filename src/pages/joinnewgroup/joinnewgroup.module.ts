import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { JoinnewgroupPage } from './joinnewgroup';

@NgModule({
  declarations: [
    JoinnewgroupPage,
  ],
  imports: [
    IonicPageModule.forChild(JoinnewgroupPage),
  ],
})
export class JoinnewgroupPageModule {}
