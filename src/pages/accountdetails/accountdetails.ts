import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController, LoadingController,ToastController } from 'ionic-angular';
import { ChamaProvider } from '../../providers/chama/chama';
import { GuarantorsPage } from '../guarantors/guarantors';
import { MinistatementPage } from '../ministatement/ministatement';
import { PendingguarantorsPage } from '../pendingguarantors/pendingguarantors';
import { GroupmembersPage } from '../groupmembers/groupmembers';

import { isNumeric } from 'rxjs/util/isNumeric';

/**
 * Generated class for the AccountdetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-accountdetails',
  templateUrl: 'accountdetails.html',
})
export class AccountdetailsPage {

  public accountNumber;
  public name;
  public dailyAmount;

  data:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl:AlertController,
    private loadingCtrl:LoadingController,public toastCtrl:ToastController,
  public Chamaservice: ChamaProvider) {

  }

  openMinistatement() {
    var navOptions = {animation: 'ios-transition'};
    this.navCtrl.push(MinistatementPage, null, navOptions);
  }

  Guarantors() {
    var navOptions = {animation: 'ios-transition'};
    this.navCtrl.push(GuarantorsPage, null, navOptions);
  }

    GroupMembers() {
      var navOptions = {animation: 'ios-transition'};
      this.navCtrl.push(GroupmembersPage, null, navOptions);
    }

  PendingGuarantors() {
    var navOptions = {animation: 'ios-transition'};
    this.navCtrl.push(PendingguarantorsPage, null, navOptions);
  }

  ionViewDidLoad() {
    this.name = this.navParams.get('name');
    this.dailyAmount = this.navParams.get('dailyAmount');
    this.accountNumber = this.navParams.get('accountNumber');
    console.log('ionViewDidLoad AccountdetailsPage'+this.accountNumber);
  }

  addGuarantor() {
  let alertme = this.alertCtrl.create({
    title: 'Add Guarantor',
    message: "Add guarantors for your account.",
    inputs: [
      {
        name: 'guarantorphone',
        placeholder: 'Guarantor Phone No: 07XX....'
      },
    ],
    buttons: [
      {
        text: 'Send',
        handler: data => {
          console.log('Send clicked');
          this.ActionAddGuarantor(data.guarantorphone);
        }
      },
      {
        text: 'Cancel',
        handler: data => {
          console.log('Cancel clicked');
        }
      }
    ]
  });
  alertme.present();
}

ActionAddGuarantor(guarantorphone) {
let loading = this.loadingCtrl.create({ content: 'Please wait...' });
loading.present();
this.Chamaservice.addguarantor(guarantorphone).then((result) => {
loading.dismissAll();
this.data = result;
console.log("ActionAddGuarantor-----"+JSON.stringify(this.data));
let status_code = this.data.code;
let message = this.data.message;
if(status_code == 1) {
  this.presentToast(message);
}
else {
  this.presentToast(message);
}

}, (err) => {
loading.dismissAll();
this.presentToast('Failed. Please try again later.');
});
}

presentToast(toastmessage) {
  let toast = this.toastCtrl.create({
    message: toastmessage,
    duration: 6000,
    position: 'bottom',
    cssClass: "toast-success"
  });

  toast.onDidDismiss(() => {
    console.log('Dismissed toast');
  });

  toast.present();
}

MakePayment() {
  let alert = this.alertCtrl.create({
    title: 'Make Payment',
    message:'Enter the amount you wish to pay. After submitting you will be presented with a prompt from MPESA to complete payment.',
    inputs: [
      {
        name: 'amount',
        placeholder: 'Amount...'
      }
    ],
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Submit',
        handler: data => {
          if (isNumeric(data.amount)) {
            // proceed with submission
            this.ProcessMakePayment(data.amount);
          } else {
            // invalid login
            return false;
          }
        }
      }
    ]
  });
  alert.present();
}

ProcessMakePayment(amount) {
  let loading = this.loadingCtrl.create({ content: 'Please wait...' });
  loading.present();
  this.Chamaservice.ProcessMakePayment(amount).then((result) => {
  loading.dismissAll();
  this.data = result;
  console.log("ProcessMakePayment-----"+JSON.stringify(this.data));
  let status_code = this.data.code;
  let message = this.data.message;
  if(status_code == 1) {
    this.presentToast(message);
  }
  else {
    this.presentToast(message);
  }
  }, (err) => {
  loading.dismissAll();
  this.presentToast('Failed. Please try again later.');
  });
}

}
