import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, App } from 'ionic-angular';

import { ChamaProvider } from '../../providers/chama/chama';
import { SpinnerDialog } from '@ionic-native/spinner-dialog';

import { LoginPage } from '../login/login';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 @IonicPage()
 @Component({
   selector: 'page-forgotpin',
   templateUrl: 'forgotpin.html',
 })
 export class ForgotpinPage {

   loading: any;
   ForgotPinData = {phone:'', idnumber:'' };
   data: any;

   constructor(private spinnerDialog: SpinnerDialog,public navCtrl: NavController, public navParams: NavParams,
   public alertCtrl: AlertController,public Chamaservice: ChamaProvider, private app:App) {

   }

   ionViewDidLoad() {
     console.log('ionViewDidLoad RegisterPage');
   }

    ActionForgotPin() {

      if(navigator.onLine === false) {
        console.log("not online")
        this.presentAlertNetwork('Internet Connection',"Please put data connection on and try again");
      }
    else if (this.ForgotPinData.idnumber === "" || this.ForgotPinData.phone === "") {
      this.presentAlert('',"All fields must be filled");
        }
        else if (isNaN(+this.ForgotPinData.phone)) {
        this.presentAlert('',"Phone number is invalid");
        }
        else if (this.ForgotPinData.phone.length !=10) {
        this.presentAlert('',"Phone number is invalid. Must be 10 characters starting with 07");
        }
        else {

    this.spinnerDialog.show();
    this.Chamaservice.Forgotpin(this.ForgotPinData).then((result) => {
      this.spinnerDialog.hide();
      this.data = result;
      localStorage.setItem('token', this.data.access_token);
      console.log("brian-----"+JSON.stringify(this.data));
      let status_code = this.data.status_code;
      let message = this.data.message;
      if(status_code ==200) {

        //clear fields
        this.ForgotPinData.phone === "";
         this.ForgotPinData.idnumber === "";

         this.app.getRootNav().setRoot(LoginPage);
          this.presentAlert('',message);
          //return;

      }
      else {
        this.presentAlert('',message);
      }

    }, (err) => {
      this.spinnerDialog.hide();
      this.presentAlert('',"Failed to reset PIN. Please try again later.");
    });

  }
  }


  presentAlert(alerttitle,alertcontent) {
   const alert = this.alertCtrl.create({
     title: alerttitle,
     subTitle: alertcontent,
     buttons: ['Ok']
   });
   alert.present();
 }

 presentAlertNetwork(alerttitle,alertcontent) {
 const alert = this.alertCtrl.create({
  title: alerttitle,
  subTitle: alertcontent,
  buttons: ['Ok']
 });
 alert.present();
 //alert.onDidDismiss(() => {  setTimeout(() => this.navCtrl.setRoot(TabsPage), 500); });
 }


 }
