import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ForgotpinPage } from './forgotpin';

@NgModule({
  declarations: [
    ForgotpinPage,
  ],
  imports: [
    IonicPageModule.forChild(ForgotpinPage),
  ],
})
export class ForgotpinPageModule {}
