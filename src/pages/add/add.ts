import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { ChamaProvider } from '../../providers/chama/chama';

import { TermsPage } from '../terms/terms';

import { DashboardPage } from '../dashboard/dashboard';

import { SpinnerDialog } from '@ionic-native/spinner-dialog';
/**
 * Generated class for the addPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 @IonicPage()
 @Component({
   selector: 'page-add',
   templateUrl: 'add.html',
 })
 export class AddPage {

   loading: any;
   addData = { firstName:'', lastName:'', phone:'', county:'', gender:'', nationalID:'' };
   data: any;

   counties:any;

   //result:any;

   public name;
   public id;
   public dailyAmount;
   public serialNumber;

   constructor(private spinnerDialog: SpinnerDialog,public navCtrl: NavController, public navParams: NavParams,
   public alertCtrl: AlertController,public Chamaservice: ChamaProvider) {

   }

   ionViewDidLoad() {
     console.log('ionViewDidLoad addPage');
     this.name = this.navParams.get('name');
     this.dailyAmount = this.navParams.get('dailyAmount');
     this.loadAllCounties();
   }

   loadAllCounties(){
     if(navigator.onLine === false) {
       console.log("not online")
       this.presentAlertNetwork('Internet Connection',"Please put data connection on and try again");
     }
     else {
     this.spinnerDialog.show();
   this.Chamaservice.loadcounties()
   .then(result => {
     this.data = result;
     this.spinnerDialog.hide();
     this.counties = this.data.result;//mm
     console.log("counties---"+this.counties)
   });
 }
   }


    openTerms() {
       this.navCtrl.push(TermsPage);
     }

    Actionadd() {

      //var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

      if(navigator.onLine === false) {
        console.log("not online")
        this.presentAlertNetwork('Internet Connection',"Please put data connection on and try again");
      }
    else if (this.addData.county === "" || this.addData.gender === "" || this.addData.nationalID === "" || this.addData.firstName === "" || this.addData.lastName === "" || this.addData.phone === "") {
      this.presentAlert('',"All fields must be filled");
        }
        else if (isNaN(+this.addData.phone)) {
        this.presentAlert('',"Phone number is invalid");
        }
        else if (this.addData.phone.length !=10) {
        this.presentAlert('',"Phone number is invalid. Must be 10 characters starting with 07");
        }
        // else if (isNaN(+this.addData.pin)) {
        // this.presentAlert('',"PIN is invalid");
        // }
        // else if (this.addData.pin.length !=4) {
        // this.presentAlert('',"PIN is invalid. Must be 4 digits");
        // }
        // else if (isNaN(+this.addData.confirmPin)) {
        // this.presentAlert('',"Confirm PIN is invalid");
        // }
        // else if (this.addData.pin.length !=4) {
        // this.presentAlert('',"Confirm PIN is invalid. Must be 4 digits");
        // }
        // else if (this.addData.pin !== this.addData.confirmPin) {
        // this.presentAlert('',"PIN fields do not match");
        // }
        else {

          // if(!this.addData.agreeterms || this.addData.agreeterms=="" || this.addData.agreeterms==null) {
          //   this.presentAlert('',"You must agree to the terms of service before completing your registration");
          //   return;
          // }

    this.spinnerDialog.show();//this.spinnerDialog.hide();
    this.Chamaservice.RegisterMember(this.addData).then((result) => {
      this.spinnerDialog.hide();
      this.data = result;
      localStorage.setItem('token', this.data.access_token);
      console.log("brian-----"+JSON.stringify(this.data));
      let status_code = this.data.code;
      let message = this.data.message;
      if(status_code == 1) {

        //clear fields
        this.addData.firstName === "";
        this.addData.lastName === "";
        this.addData.gender === "";
        this.addData.phone === "";
        this.addData.nationalID === "";

          this.presentAlert('',message);
          this.navCtrl.setRoot(DashboardPage);

      }
      else {
        this.presentAlert('',message);
      }

    }, (err) => {
      this.spinnerDialog.hide();
      this.presentAlert('',"Failed to register new member. Please try again later.");
    });

  }
  }


  presentAlert(alerttitle,alertcontent) {
   const alert = this.alertCtrl.create({
     title: alerttitle,
     subTitle: alertcontent,
     buttons: ['Ok']
   });
   alert.present();
 }

 presentAlertNetwork(alerttitle,alertcontent) {
 const alert = this.alertCtrl.create({
  title: alerttitle,
  subTitle: alertcontent,
  buttons: ['Ok']
 });
 alert.present();
 //alert.onDidDismiss(() => {  setTimeout(() => this.navCtrl.setRoot(TabsPage), 500); });
 }


 }
