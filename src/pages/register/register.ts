import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { ChamaProvider } from '../../providers/chama/chama';
import { LoginPage } from '../login/login';
import { TermsPage } from '../terms/terms';
//import { SetpinPage } from '../setpin/setpin';
//import { AvailablegroupsPage } from '../availablegroups/availablegroups';
import { SpinnerDialog } from '@ionic-native/spinner-dialog';

//import { OtpPage } from '../otp/otp';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 @IonicPage()
 @Component({
   selector: 'page-register',
   templateUrl: 'register.html',
 })
 export class RegisterPage {

   loading: any;
   RegisterData = { firstName:'', lastName:'', phone:'', county:'', gender:'', nationalID:'', pin:'', confirmPin:'' };
   data: any;

   genders:any;
   counties:any;

   public name;
   public id;
   public dailyAmount;
   public serialNumber;

   //name:any;
   //dailyAmount:any;

   constructor(private spinnerDialog: SpinnerDialog,public navCtrl: NavController, public navParams: NavParams,
   public alertCtrl: AlertController,public Chamaservice: ChamaProvider) {

   }

   ionViewDidLoad() {
     this.name = this.navParams.get('name');
     this.dailyAmount = this.navParams.get('dailyAmount');
     console.log('ionViewDidLoad RegisterPage---'+this.name);
     this.loadAllCounties();
   }

   loadAllCounties(){
     if(navigator.onLine === false) {
       console.log("not online")
       this.presentAlertNetwork('Internet Connection',"Please put data connection on and try again");
     }
     else {
     this.spinnerDialog.show();
   this.Chamaservice.loadcounties()
   .then(result => {
     this.data = result;
     this.spinnerDialog.hide();
     this.counties = this.data.result;//mm
     console.log("counties---"+this.counties)
   });
 }
   }


   openLogin() {
      this.navCtrl.push(LoginPage);
    }

    // openGroups() {
    //    this.navCtrl.push(AvailablegroupsPage);
    //  }

    // openSetpin() {
    //    this.navCtrl.push(SetpinPage);
    //  }

    openTerms() {
       this.navCtrl.push(TermsPage);
     }

    ActionRegister() {

      //var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

      if(navigator.onLine === false) {
        console.log("not online")
        this.presentAlertNetwork('Internet Connection',"Please put data connection on and try again");
      }
    else if (this.RegisterData.county === "" || this.RegisterData.gender === "" || this.RegisterData.pin === "" || this.RegisterData.confirmPin === "" || this.RegisterData.nationalID === "" || this.RegisterData.firstName === "" || this.RegisterData.lastName === "" || this.RegisterData.phone === "") {
      this.presentAlert('',"All fields must be filled");
        }
        else if (isNaN(+this.RegisterData.phone)) {
        this.presentAlert('',"Phone number is invalid");
        }
        else if (this.RegisterData.phone.length !=10) {
        this.presentAlert('',"Phone number is invalid. Must be 10 characters starting with 07");
        }
        else if (isNaN(+this.RegisterData.pin)) {
        this.presentAlert('',"PIN is invalid");
        }
        else if (this.RegisterData.pin.length !=4) {
        this.presentAlert('',"PIN is invalid. Must be 4 digits");
        }
        else if (isNaN(+this.RegisterData.confirmPin)) {
        this.presentAlert('',"Confirm PIN is invalid");
        }
        else if (this.RegisterData.pin.length !=4) {
        this.presentAlert('',"Confirm PIN is invalid. Must be 4 digits");
        }
        else if (this.RegisterData.pin !== this.RegisterData.confirmPin) {
        this.presentAlert('',"PIN fields do not match");
        }
        else {

          // if(!this.RegisterData.agreeterms || this.RegisterData.agreeterms=="" || this.RegisterData.agreeterms==null) {
          //   this.presentAlert('',"You must agree to the terms of service before completing your registration");
          //   return;
          // }

    this.spinnerDialog.show();//this.spinnerDialog.hide();
    this.Chamaservice.Register(this.RegisterData).then((result) => {
      this.spinnerDialog.hide();
      this.data = result;
      localStorage.setItem('token', this.data.access_token);
      console.log("brian-----"+JSON.stringify(this.data));
      let status_code = this.data.code;
      let message = this.data.message;
      if(status_code ==1) {

        localStorage.setItem("DigitalTrustcustomerphone",this.data.result.phone);

        //clear fields
        this.RegisterData.firstName === "";
        this.RegisterData.lastName === "";
        this.RegisterData.gender === "";
        this.RegisterData.phone === "";
        //this.RegisterData.email === "";
         this.RegisterData.nationalID === "";
        // this.RegisterData.cpassword === "";

        localStorage.setItem("DigitalTrustuserstatus","registered")

          this.navCtrl.push(LoginPage);
          this.presentAlert('',message);
          //return;

      }
      else {
        this.presentAlert('',message);
      }

    }, (err) => {
      this.spinnerDialog.hide();
      this.presentAlert('',"Sorry, we are experiencing a technical challenge. Try again later.");
    });

  }
  }


  presentAlert(alerttitle,alertcontent) {
   const alert = this.alertCtrl.create({
     title: alerttitle,
     subTitle: alertcontent,
     buttons: ['Ok']
   });
   alert.present();
 }

 presentAlertNetwork(alerttitle,alertcontent) {
 const alert = this.alertCtrl.create({
  title: alerttitle,
  subTitle: alertcontent,
  buttons: ['Ok']
 });
 alert.present();
 //alert.onDidDismiss(() => {  setTimeout(() => this.navCtrl.setRoot(TabsPage), 500); });
 }


 }
