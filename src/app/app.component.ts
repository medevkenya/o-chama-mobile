import { Component } from '@angular/core';
import { Platform,AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ChamaProvider } from '../providers/chama/chama';

import { TranslateService} from '../../node_modules/@ngx-translate/core';

import { HomePage } from '../pages/home/home';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  data:any;

  constructor(platform: Platform, public translate:TranslateService, statusBar: StatusBar, splashScreen: SplashScreen,public alertCtrl: AlertController,public Chamaservice: ChamaProvider) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      this.translate.setDefaultLang('en');
      this.translate.use('en');
      //this.checkForUpdate();
    });
  }


  checkForUpdate() {
  this.Chamaservice.checkForUpdate().then((result) => {
  this.data = result;
  console.log("checkForUpdate---"+JSON.stringify(this.data));
  if(this.data.code == 1) {
    this.presentAlert();
  }
  }, (err) => {
  //this.presentToast('fcm data--'+this.data);//
  });
  }

  presentAlert() {
  const alert = this.alertCtrl.create({
  title: 'Update App',
  subTitle: 'There is a new update in Google Play Store. Press Ok to update now. Thank you.',
  buttons: ['Ok']
  });
  alert.present();
  alert.onDidDismiss(() => {  window.open('https://play.google.com/store/apps/details?id=DigitalTrust.mobile.com&hl=en_US', '_system'); });
  }

}
