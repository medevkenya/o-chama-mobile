import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { SocialSharing } from '@ionic-native/social-sharing';
import { SpinnerDialog } from '@ionic-native/spinner-dialog';
import { Network } from '@ionic-native/network';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { ChangelanguagePageModule } from '../pages/changelanguage/changelanguage.module';

import { AddPageModule } from '../pages/add/add.module';
import { PreaddPageModule } from '../pages/preadd/preadd.module';
import { LoginPageModule } from '../pages/login/login.module';
import { OtpPageModule } from '../pages/otp/otp.module';
import { RegisterPageModule } from '../pages/register/register.module';
import { HelpPageModule } from '../pages/help/help.module';
import { AboutPageModule } from '../pages/about/about.module';
import { MembersPageModule } from '../pages/members/members.module';
import { MyaccountsPageModule } from '../pages/myaccounts/myaccounts.module';
import { MydepositsPageModule } from '../pages/mydeposits/mydeposits.module';
import { MymessagesPageModule } from '../pages/mymessages/mymessages.module';
import { MypaymentsPageModule } from '../pages/mypayments/mypayments.module';

import { PayoutsPageModule } from '../pages/payouts/payouts.module';
import { TermsPageModule } from '../pages/terms/terms.module';
import { ProfilePageModule } from '../pages/profile/profile.module';
import { TabsPage } from '../pages/tabs/tabs';
import { DashboardPageModule } from '../pages/dashboard/dashboard.module';

import { AvailablegroupsPageModule } from '../pages/availablegroups/availablegroups.module';
import { FaqsPageModule } from '../pages/faqs/faqs.module';
import { SetpinPageModule } from '../pages/setpin/setpin.module';
import { ChamaProvider } from '../providers/chama/chama';

import { AccountdetailsPageModule } from '../pages/accountdetails/accountdetails.module';

import { JoinnewgroupPageModule } from '../pages/joinnewgroup/joinnewgroup.module';
import { PrivacyPageModule } from '../pages/privacy/privacy.module';

import { GuarantorsPageModule } from '../pages/guarantors/guarantors.module';
import { PendingguarantorsPageModule } from '../pages/pendingguarantors/pendingguarantors.module';
//import { PayoutsPageModule } from '../pages/payouts/payouts.module';

import { ChangepinPageModule } from '../pages/changepin/changepin.module';
import { WithdrawcashPageModule } from '../pages/withdrawcash/withdrawcash.module';
import { MinistatementPageModule } from '../pages/ministatement/ministatement.module';
import { ForgotpinPageModule } from '../pages/forgotpin/forgotpin.module';

import { GroupmembersPageModule } from '../pages/groupmembers/groupmembers.module';
import { TransferfundsPageModule } from '../pages/transferfunds/transferfunds.module';
import { WalletPageModule } from '../pages/wallet/wallet.module';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

var config = {
  backButtonText: '',
  backButtonIcon: 'md-arrow-back',
  iconMode: 'ios',
  pageTransition: 'ios'
  // mode:'ios'
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,TabsPage
  ],
  imports: [
    PendingguarantorsPageModule,GroupmembersPageModule,TransferfundsPageModule,WalletPageModule,
    ChangepinPageModule,WithdrawcashPageModule,MinistatementPageModule,ForgotpinPageModule,
    HttpModule,BrowserModule,LoginPageModule, RegisterPageModule, HelpPageModule,AboutPageModule,MembersPageModule,
    MyaccountsPageModule, MydepositsPageModule,MymessagesPageModule,MypaymentsPageModule,
    PayoutsPageModule,TermsPageModule,DashboardPageModule,AvailablegroupsPageModule,
    FaqsPageModule,ProfilePageModule,SetpinPageModule,AccountdetailsPageModule,
    JoinnewgroupPageModule,PrivacyPageModule,ChangelanguagePageModule,PreaddPageModule,AddPageModule,
    OtpPageModule,GuarantorsPageModule,PayoutsPageModule,
    IonicModule.forRoot(MyApp,config),
    HttpClientModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,TabsPage
  ],
  providers: [
    StatusBar,SocialSharing,SpinnerDialog,Network,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ChamaProvider
  ]
})
export class AppModule {}
