import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {Headers} from '@angular/http';

/*
  Generated class for the ChamaProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ChamaProvider {

private DigitalTrustendpointurl = 'https://api.digitaltrust.co.ke/api/';
private DigitalTrustPaymentendpointurl = 'https://payments.digitaltrust.com/api/';

constructor(public http: Http) {
  console.log('Hello ChamaProvider Provider');
}

public chu:any;
checkForUpdate() {
//let id = localStorage.getItem("memescustomerid");
return new Promise(resolve => {
  this.http.get(this.DigitalTrustendpointurl+'checkForUpdate/0.0.4')
    .map(res => res.json())
    .subscribe(data => {
      this.chu = data;
      console.log("checkForUpdate api---"+this.chu)
      resolve(this.chu);
    });
});
}

requestOtp(credentials) {
let phone = credentials.phone;
let pin = credentials.pin;
let localinfo = { phonenumber:phone,pin:pin, hashedKey:'1' };
console.log("sentbody23232322 requestOtp--"+JSON.stringify(localinfo));
return new Promise((resolve, reject) => {
let headers = new Headers();
headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
this.http.post(this.DigitalTrustendpointurl+'requestOtp', JSON.stringify(localinfo), {headers: headers})
  .subscribe(res => {
    //console.log("brian2-----"+res.json());
    resolve(res.json());
  }, (err) => {
    //console.log("brian3-----"+err);
    reject(err);
  });
});
}

ChangePin(credentials) {
  let phone = localStorage.getItem("DigitalTrustcustomerphone");
  //let confirmPin = credentials.confirmPin;
  let newPin = credentials.pin;
  let currentpin = credentials.currentpin;
  let localinfo = { phone:phone,newPin:newPin, pin:currentpin };
  console.log("sentbody23232322 requestOtp--"+JSON.stringify(localinfo));
  return new Promise((resolve, reject) => {
  let headers = new Headers();
  headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
  this.http.post(this.DigitalTrustendpointurl+'members/changePin', JSON.stringify(localinfo), {headers: headers})
  .subscribe(res => {
    //console.log("brian2-----"+res.json());
    resolve(res.json());
  }, (err) => {
    //console.log("brian3-----"+err);
    reject(err);
  });
  });
}

CheckWalletBalance() {
  let phone = localStorage.getItem("DigitalTrustcustomerphone");
  let localinfo = { phone:phone };
  console.log("sentbody23232322 CheckWalletBalance--"+JSON.stringify(localinfo));
  return new Promise((resolve, reject) => {
  let headers = new Headers();
  headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
  this.http.post(this.DigitalTrustendpointurl+'payments/checkWalletBalance', JSON.stringify(localinfo), {headers: headers})
  .subscribe(res => {
    //console.log("brian2-----"+res.json());
    resolve(res.json());
  }, (err) => {
    //console.log("brian3-----"+err);
    reject(err);
  });
  });
}

Login(credentials) {
  let phone = credentials.phone;
  let pin = credentials.pin;
  let localinfo = { phone:phone, pin:pin };
  console.log("sentbody23232322 login--"+JSON.stringify(localinfo));
  return new Promise((resolve, reject) => {
  let headers = new Headers();
  headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
  this.http.post(this.DigitalTrustendpointurl+'members/login', JSON.stringify(localinfo), {headers: headers})
    .subscribe(res => {
      console.log("brian2-----"+JSON.stringify(res.json()));
      resolve(res.json());
    }, (err) => {
      console.log("brian3-----"+JSON.stringify(err));
      reject(err);
    });
  });
}

Forgotpin(credentials) {
  let phone = credentials.phone;
  let nationalID = credentials.nationalID;
  let localinfo = { phone:phone, nationalID:nationalID };
  console.log("sentbody23232322 forgotpin--"+JSON.stringify(localinfo));
  return new Promise((resolve, reject) => {
  let headers = new Headers();
  headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
  this.http.post(this.DigitalTrustendpointurl+'forgotPin', JSON.stringify(localinfo), {headers: headers})
    .subscribe(res => {
      console.log("brian2-----"+JSON.stringify(res.json()));
      resolve(res.json());
    }, (err) => {
      console.log("brian3-----"+JSON.stringify(err));
      reject(err);
    });
  });
}

Register(credentials) {
//let group = localStorage.getItem('DigitalTrustactivesubgroupid');
let group = +localStorage.getItem('DigitalTrustactivegroupid');
let phone = credentials.phone;
let firstName = credentials.firstName;
let lastName = credentials.lastName;
//let middleName = credentials.middleName;
let nationalID = credentials.nationalID;
let county = credentials.county;
let gender = credentials.gender;
let pin = credentials.pin;
let confirmPin = credentials.confirmPin;
let localinfo = { registeredBy:"0", middleName:"", phone:phone, pin:pin, confirmPin:confirmPin, group:group, nationalID:nationalID, firstName:firstName, lastName:lastName, county:county, gender:gender };
console.log("sentbody23232322--"+JSON.stringify(localinfo));
return new Promise((resolve, reject) => {
  let headers = new Headers();
  headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    this.http.post(this.DigitalTrustendpointurl+'members/store', JSON.stringify(localinfo), {headers: headers})
      .subscribe(res => {
        console.log("brian2-----"+JSON.stringify(res.json()));
        resolve(res.json());
      }, (err) => {
        console.log("brian3-----"+JSON.stringify(err));
        reject(err);
      });
});
}

RegisterMember(credentials) {
let group = +localStorage.getItem('DigitalTrustactivegroupid');
let registeredbyphone = 1;//localStorage.getItem("DigitalTrustcustomerphone");
let phone = credentials.phone;
let firstName = credentials.firstName;
let lastName = credentials.lastName;
let county = credentials.county;
let gender = credentials.gender;
let nationalID = credentials.nationalID;
let localinfo = { nationalID:nationalID, registeredBy:registeredbyphone, phone:phone, group:group, middleName:"", firstName:firstName, lastName:lastName, county:county, gender:gender };
console.log("sentbody23232322--"+JSON.stringify(localinfo));
return new Promise((resolve, reject) => {
  let headers = new Headers();
  headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    this.http.post(this.DigitalTrustendpointurl+'members/registerMember', JSON.stringify(localinfo), {headers: headers})
    .subscribe(res => {
      console.log("brian2-----"+JSON.stringify(res.json()));
      resolve(res.json());
    }, (err) => {
      console.log("brian3-----"+JSON.stringify(err));
      reject(err);
    });
});
}

Profile(credentials) {
let phone = localStorage.getItem("DigitalTrustcustomerphone");
let firstName = credentials.firstName;
let lastName = credentials.lastName;
let countyname = localStorage.getItem("DigitalTrustcustomercountyname");//credentials.countyname;
let gender = credentials.gender;
let localinfo = { phone:phone, firstName:firstName, lastName:lastName, countyname:countyname, gender:gender };
console.log("sentbody23232322--"+JSON.stringify(localinfo));
return new Promise((resolve, reject) => {
  let headers = new Headers();
  headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    this.http.post(this.DigitalTrustendpointurl+'editprofile', JSON.stringify(localinfo), {headers: headers})
      .subscribe(res => {
        //console.log("brian2-----"+res.json());
        resolve(res.json());
      }, (err) => {
        //console.log("brian3-----"+err);
        reject(err);
      });
});
}

WithdrawCash(credentials) {
let phone = localStorage.getItem("DigitalTrustcustomerphone");
let amount = credentials.amount;
let pin = credentials.pin;
let localinfo = { phone:phone, amount:amount, pin:pin };
console.log("sentbody23232322--"+JSON.stringify(localinfo));
return new Promise((resolve, reject) => {
  let headers = new Headers();
  headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    this.http.post(this.DigitalTrustendpointurl+'payments/withdraw', JSON.stringify(localinfo), {headers: headers})
      .subscribe(res => {
        //console.log("brian2-----"+res.json());
        resolve(res.json());
      }, (err) => {
        //console.log("brian3-----"+err);
        reject(err);
      });
});
}

// Addmember(credentials) {
// let subgroupid = localStorage.getItem('DigitalTrustactivesubgroupid');
// let registeredbyphone = localStorage.getItem("DigitalTrustcustomerphone");
// let registeredbyaccountnumber = localStorage.getItem("DigitalTrustactiveaccountnumber");
// let phone = credentials.phone;
// let firstName = credentials.firstName;
// let lastName = credentials.lastName;
// let type = "0";//credentials.type;
// let countyname = credentials.countyname;
// let gender = credentials.gender;
// let nationalID = credentials.nationalID;
// let localinfo = { phone:phone, nationalID:nationalID,  registeredbyphone:registeredbyphone, registeredbyaccountnumber:registeredbyaccountnumber, subgroupid:subgroupid,firstName:firstName, lastName:lastName, type:type, county_name:countyname, gender:gender };
// console.log("sentbody23232322--"+JSON.stringify(localinfo));
// return new Promise((resolve, reject) => {
//   let headers = new Headers();
//   headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
//     this.http.post(this.DigitalTrustendpointurl+'addmember', JSON.stringify(localinfo), {headers: headers})
//       .subscribe(res => {
//         //console.log("brian2-----"+res.json());
//         resolve(res.json());
//       }, (err) => {
//         //console.log("brian3-----"+err);
//         reject(err);
//       });
// });
// }

Setpin(credentials) {
let phone = localStorage.getItem("DigitalTrustcustomerphone");
let pin = credentials.pin;
let localinfo = { phone:phone, pin:pin };
console.log("sentbody23232322--"+JSON.stringify(localinfo));
return new Promise((resolve, reject) => {
  let headers = new Headers();
  headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    this.http.post(this.DigitalTrustendpointurl+'members/setPin', JSON.stringify(localinfo), {headers: headers})
      .subscribe(res => {
        //console.log("brian2-----"+res.json());
        resolve(res.json());
      }, (err) => {
        //console.log("brian3-----"+err);
        reject(err);
      });
});
}

Joinnewgroup(group) {
let phone = localStorage.getItem("DigitalTrustcustomerphone");
let localinfo = { phone:phone, group:group };
console.log("sentbody23232322--"+JSON.stringify(localinfo));
return new Promise((resolve, reject) => {
  let headers = new Headers();
  headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    this.http.post(this.DigitalTrustendpointurl+'members/joinGroup', JSON.stringify(localinfo), {headers: headers})
      .subscribe(res => {
        //console.log("brian2-----"+res.json());
        resolve(res.json());
      }, (err) => {
        //console.log("brian3-----"+err);
        reject(err);
      });
});
}

actOnGuarantorRequest(id,status) {
//let phone = localStorage.getItem("DigitalTrustcustomerphone"); //phone:phone, id:id,
let localinfo = { status:status };
console.log("sentbody23232322--"+JSON.stringify(localinfo));
return new Promise((resolve, reject) => {
  let headers = new Headers();
  headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    this.http.post(this.DigitalTrustendpointurl+'guarantors/update/'+id+'', JSON.stringify(localinfo), {headers: headers})
    .subscribe(res => {
      console.log("brian2-----"+JSON.stringify(res.json()));
      resolve(res.json());
    }, (err) => {
      console.log("brian3-----"+JSON.stringify(err));
      reject(err);
    });
});
}

addguarantor(guarantorPhone) {
let phone = localStorage.getItem("DigitalTrustcustomerphone");
let accountNumber = localStorage.getItem("DigitalTrustactiveaccountnumber");
let localinfo = { phone:phone, guarantorPhone:guarantorPhone, accountNumber:accountNumber };
console.log("sentbody23232322--"+JSON.stringify(localinfo));
return new Promise((resolve, reject) => {
  let headers = new Headers();
  headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    this.http.post(this.DigitalTrustendpointurl+'guarantors/add', JSON.stringify(localinfo), {headers: headers})
      .subscribe(res => {
        //console.log("brian2-----"+res.json());
        resolve(res.json());
      }, (err) => {
        //console.log("brian3-----"+err);
        reject(err);
      });
});
}

getministatement() {
let phone = localStorage.getItem("DigitalTrustcustomerphone");
let localinfo = { phone:phone, limit:10 };
console.log("sentbody23232322--"+JSON.stringify(localinfo));
return new Promise((resolve, reject) => {
  let headers = new Headers();
  headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    this.http.post(this.DigitalTrustendpointurl+'payments/miniStatement', JSON.stringify(localinfo), {headers: headers})
      .subscribe(res => {
        //console.log("brian2-----"+res.json());
        resolve(res.json());
      }, (err) => {
        //console.log("brian3-----"+err);
        reject(err);
      });
});
}

TransferCash(credentials) {
let phone = localStorage.getItem("DigitalTrustcustomerphone");
let amount = credentials.amount;
let pin = credentials.pin;
let localinfo = { phone:phone, amount:amount, pin:pin };
console.log("sentbody23232322--"+JSON.stringify(localinfo));
return new Promise((resolve, reject) => {
  let headers = new Headers();
  headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    this.http.post(this.DigitalTrustendpointurl+'payments/transferFunds', JSON.stringify(localinfo), {headers: headers})
      .subscribe(res => {
        //console.log("brian2-----"+res.json());
        resolve(res.json());
      }, (err) => {
        //console.log("brian3-----"+err);
        reject(err);
      });
});
}

loadmyguarantors() {
let phone = localStorage.getItem("DigitalTrustcustomerphone");
let accountNumber = localStorage.getItem("DigitalTrustactiveaccountnumber");
let localinfo = { phone:phone, accountNumber:accountNumber };
console.log("sentbody23232322--"+JSON.stringify(localinfo));
return new Promise((resolve, reject) => {
  let headers = new Headers();
  headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    this.http.post(this.DigitalTrustendpointurl+'guarantors', JSON.stringify(localinfo), {headers: headers})
      .subscribe(res => {
        //console.log("brian2-----"+res.json());
        resolve(res.json());
      }, (err) => {
        //console.log("brian3-----"+err);
        reject(err);
      });
});
}

loadgroupmembers() {
let phone = localStorage.getItem("DigitalTrustcustomerphone");
let accountNumber = localStorage.getItem("DigitalTrustactiveaccountnumber");
let localinfo = { phone:phone, accountNumber:accountNumber };
console.log("sentbody23232322--"+JSON.stringify(localinfo));
return new Promise((resolve, reject) => {
  let headers = new Headers();
  headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    this.http.post(this.DigitalTrustendpointurl+'members/fellowMembers', JSON.stringify(localinfo), {headers: headers})
      .subscribe(res => {
        //console.log("brian2-----"+res.json());
        resolve(res.json());
      }, (err) => {
        //console.log("brian3-----"+err);
        reject(err);
      });
});
}

ProcessMakePayment(amount) {
let phone = localStorage.getItem("DigitalTrustcustomerphone");
let accountNumber = localStorage.getItem("DigitalTrustactiveaccountnumber");
let localinfo = { phone:phone, accountNumber:accountNumber, amount:amount };
console.log("sentbody23232322--"+JSON.stringify(localinfo));
return new Promise((resolve, reject) => {
  let headers = new Headers();
  headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    this.http.post(this.DigitalTrustPaymentendpointurl+'requestSTKpush', JSON.stringify(localinfo), {headers: headers})
      .subscribe(res => {
        //console.log("brian2-----"+res.json());
        resolve(res.json());
      }, (err) => {
        //console.log("brian3-----"+err);
        reject(err);
      });
});
}

loadmymessages() {
let phone = localStorage.getItem("DigitalTrustcustomerphone");
let localinfo = { phone:phone };
console.log("sentbody23232322--"+JSON.stringify(localinfo));
return new Promise((resolve, reject) => {
  let headers = new Headers();
  headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    this.http.post(this.DigitalTrustendpointurl+'members/memberMessages', JSON.stringify(localinfo), {headers: headers})
      .subscribe(res => {
        //console.log("brian2-----"+res.json());
        resolve(res.json());
      }, (err) => {
        //console.log("brian3-----"+err);
        reject(err);
      });
});
}

public counties1:any;
loadcounties() {
  return new Promise(resolve => {
    this.http.get(''+this.DigitalTrustendpointurl+'counties')
      .map(res => res.json())
      .subscribe(data => {
        this.counties1 = data;
        console.log("countries api---"+this.counties1)
        resolve(this.counties1);
      });
  });
}

loadavailablegroups(): Observable<string[]> {
  return this.http.get(this.DigitalTrustendpointurl+'groups/availableGroups')
                  .map(this.extractData)
                  .catch(this.handleError);
}

loadpreadd(): Observable<string[]> {
  return this.http.get(this.DigitalTrustendpointurl+'groups/availableGroups')
                  .map(this.extractData)
                  .catch(this.handleError);
}

loadjoinnewgroup(): Observable<string[]> {
  return this.http.get(this.DigitalTrustendpointurl+'groups/availableGroups')
                  .map(this.extractData)
                  .catch(this.handleError);
}

loadmyaccounts(): Observable<string[]> {
  let phone = localStorage.getItem('DigitalTrustcustomerphone');
  return this.http.get(this.DigitalTrustendpointurl+'members/accounts/'+phone)
                  .map(this.extractData)
                  .catch(this.handleError);
}

// loadgroupmembers(): Observable<string[]> {
//   let accountNumber = localStorage.getItem('DigitalTrustactiveaccountnumber');
//   return this.http.get(this.DigitalTrustendpointurl+'members/fellowMembers/'+accountNumber)
//                   .map(this.extractData)
//                   .catch(this.handleError);
// }

// loadmymessages(): Observable<string[]> {
//   let phone = localStorage.getItem('DigitalTrustcustomerphone');
//   return this.http.get(this.DigitalTrustendpointurl+'messages/'+phone)
//                   .map(this.extractData)
//                   .catch(this.handleError);
// }

loadbranches(page): Observable<string[]> {
  return this.http.get(this.DigitalTrustendpointurl+'getbranches/'+page)
                  .map(this.extractData)
                  .catch(this.handleError);
}

loadnews(page): Observable<string[]> {
  return this.http.get(this.DigitalTrustendpointurl+'getnews/'+page)
                  .map(this.extractData)
                  .catch(this.handleError);
}

loadpendingrequests(): Observable<string[]> {
  let phone = localStorage.getItem('DigitalTrustcustomerphone');
  return this.http.get(this.DigitalTrustendpointurl+'guarantors/pendingRequests/'+phone)
                  .map(this.extractData)
                  .catch(this.handleError);
}

// loadmyguarantors(): Observable<string[]> {
//   let phone = localStorage.getItem('DigitalTrustcustomerphone');
//   return this.http.get(this.DigitalTrustendpointurl+'guarantors/'+phone)
//                   .map(this.extractData)
//                   .catch(this.handleError);
// }

loadacceptedrequests(page): Observable<string[]> {
  let phone = localStorage.getItem('DigitalTrustcustomerphone');
  return this.http.get(this.DigitalTrustendpointurl+'acceptedGuarantors/'+phone+'/'+page)
                  .map(this.extractData)
                  .catch(this.handleError);
}

loaddeclinedrequests(page): Observable<string[]> {
  let phone = localStorage.getItem('DigitalTrustcustomerphone');
  return this.http.get(this.DigitalTrustendpointurl+'declinedGuarantors/'+phone+'/'+page)
                  .map(this.extractData)
                  .catch(this.handleError);
}

loadmypayments(page) {
let accountnumber = localStorage.getItem("DigitalTrustactiveaccountnumber");
let localinfo = { accountnumber:accountnumber, pageNumber:page };
console.log("sentbody23232322--"+JSON.stringify(localinfo));
return new Promise((resolve, reject) => {
  let headers = new Headers();
  headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    this.http.post(this.DigitalTrustendpointurl+'getmypayments', JSON.stringify(localinfo), {headers: headers})
      .subscribe(res => {
        //console.log("brian2-----"+res.json());
        resolve(res.json());
      }, (err) => {
        //console.log("brian3-----"+err);
        reject(err);
      });
});
}

private extractData(res: Response) {
let body = res.json();
return body || { };
}
private handleError (error: Response | any) {
let errMsg: string;
if (error instanceof Response) {
  const body = error.json() || '';
  const err = body.error || JSON.stringify(body);
  errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
} else {
  errMsg = error.message ? error.message : error.toString();
}
console.error(errMsg);
return Observable.throw(errMsg);
}

}
